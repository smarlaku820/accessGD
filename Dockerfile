FROM python:3.8.7

RUN apt-get update && apt-get install sudo

RUN mkdir /accessGD

WORKDIR "/accessGD"

RUN python3 -m venv venv

COPY mycreds.txt requirements.txt update_splunk_license.py /accessGD/
 
RUN . venv/bin/activate && pip install -r requirements.txt