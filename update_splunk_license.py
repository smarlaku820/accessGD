from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
import pexpect
import sys


def fire_splunk_command(command_str, password):
    res = pexpect.spawn(command_str)
    res.expect('Password')
    res.sendline(password)
    res_output = res.read()
    print(res_output.decode("utf-8"))


if __name__ == "__main__":
    try:
        password = sys.argv[1]
    except IndexError:
        print("Oops! System Password is Required to Execute Splunk Commands ")
        exit(1)

    gauth = GoogleAuth()

    # Try to load saved client credentials
    gauth.LoadCredentialsFile("mycreds.txt")

    if gauth.credentials is None:
        # Authenticate if they're not there
        gauth.LocalWebserverAuth()
    elif gauth.access_token_expired:
        # Refresh them if expired
        gauth.Refresh()
    else:
        # Initialize the saved creds
        gauth.Authorize()
    # Save the current credentials to a file
    gauth.SaveCredentialsFile("mycreds.txt")

    drive = GoogleDrive(gauth)
    # Get the file list from the shared
    fl = drive.ListFile({'q':"'19ghIMIZ4kGn3g-U-jDCXgK-p8GyV0pYT' in parents and trashed=false"}).GetList()

    # Download the license files
    for i, file in enumerate(sorted(fl, key = lambda x: x['title']), start=1):
        print('Downloading {} from GDrive ({}/{})'.format(file['title'], i, len(fl)))
        file.GetContentFile(file['title'])

    commands = ['sudo /opt/splunk/bin/splunk stop',
                'sudo /opt/splunk/bin/splunk add license splunk.license.big.license',
                'sudo /opt/splunk/bin/splunk start']

    for cmd in commands:
        fire_splunk_command(cmd, password)